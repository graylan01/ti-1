 
**TI-1 Operational Code of Use**

**1. Ownership and Licensing:**
   - Titanium 1 TI-1 simulation access is owned and redistrubited by Graylan Janulis and is provided under the GNU General Public License Version 2.0 (GPL-2.0).

**2. Permitted Uses:**
   - The Device may be used for quantum computing research, development, and practical applications across scientific, industrial, and educational domains.
   - Modification of the Device’s hardware and software is permitted under the terms of GPL-2.0.

**3. Distribution and Redistribution:**
   - The Device or any modified versions of it may be distributed under the terms of GPL-2.0, facilitating widespread access and collaboration.
   - Users receiving the Device or modified versions must also receive a copy of the GPL-2.0 license.

**4. Patents and Patent Grants:**
   - Any patents associated with the Device are licensed under GPL-2.0, ensuring that users have the rights necessary to utilize and innovate with the Device.

**5. No Warranty:**
   - The Device is provided without warranties or guarantees of any kind. Users assume all risks associated with its use and modifications.

**6. Attribution and Acknowledgment:**
   - Any publications, research papers, or derivative works resulting from the use of the Device should acknowledge its use and credit Graylan Janulis as the original creator.

**7. Human Freedom and Global Benefit:**
   - The Device is intended to promote human freedom and advance global progress across all disciplines and industries.
   - Its use is encouraged for applications that enhance human welfare, promote environmental sustainability, support humanitarian efforts, and foster international collaboration.

**8. Compliance:**
   - Users of the Device must comply with all terms and conditions specified in the GPL-2.0 license, ensuring the preservation of open-source principles and community spirit.
 